#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>
#include <exception>
#include <utility>
#include <random>
#include <chrono>
struct sqmatrix_t;

struct vec_t
{
    int     dim;
    double  *data;

    vec_t();
    vec_t(const int dim);

    vec_t(const vec_t& that);
    vec_t(vec_t&& that);
    
    ~vec_t();

    vec_t& operator=(const vec_t& that);
    vec_t& operator=(vec_t&& that);

    void setdim(const int dim);

    double&         operator[](const int idx);
    const double&   operator[](const int idx) const;

    vec_t operator*(const float b) const;
    vec_t operator/(const float b) const;

    vec_t operator+(const vec_t& that) const;
    vec_t operator-(const vec_t& that) const;
};

std::ostream& operator<<(std::ostream& os, const vec_t& v);
std::istream& operator>>(std::istream& is,       vec_t& v);

enum norm_t
{
    EUCLID = 1
};

double norm(const vec_t& v, const norm_t norm = norm_t::EUCLID);

vec_t::vec_t() 
{ 
    this->dim = 0; 
    this->data = nullptr; 
}

vec_t::vec_t(const int dim)
{
    this->dim = dim;
    this->data = new double[this->dim]{};
}

vec_t::vec_t(const vec_t& that)
{
    new(this) vec_t(that.dim);
    for(int i = 0; i < this->dim; ++i) {
        this->data[i] = that.data[i];
    }
}

vec_t::vec_t(vec_t&& that)
{
    new(this) vec_t;
    std::swap(this->data, that.data);
    std::swap(this->dim,  that.dim);   
}

vec_t::~vec_t()
{
    if(this->data != nullptr) {
        delete[] this->data;
    }
}

vec_t& vec_t::operator=(const vec_t& that)
{
    if(this->dim != that.dim) {
        throw std::runtime_error("Non-compatible assignment");
    }

    for(int i = 0; i < this->dim; ++i) {
        this->data[i] = that.data[i];
    }
    return *this;
}

vec_t& vec_t::operator=(vec_t&& that)
{
    if(this->dim != that.dim) {
        throw std::runtime_error("Non-compatible assignment");
    }
    std::swap(this->data, that.data);
    std::swap(this->dim,  that.dim);
    return *this;
}

void vec_t::setdim(const int dim)
{
    if((this->dim != dim) && (dim > 0)) {
        this->~vec_t();
        new(this) vec_t(dim);
    }
}

double& vec_t::operator[](const int idx)
{
    return this->data[idx];
}

const double& vec_t::operator[](const int idx) const
{
    return this->data[idx];
}

vec_t vec_t::operator*(const float b) const
{
    vec_t out(this->dim);
    for(int i = 0; i < out.dim; ++i) {
        out.data[i] = this->data[i] * b;
    }
    return out;
}

vec_t vec_t::operator/(const float b) const
{
    return (*this) * (1/b);
}

vec_t vec_t::operator+(const vec_t& that) const
{
    if(this->dim != that.dim) {
        throw std::runtime_error("Non-compatible binary operator");
    }

    vec_t out(this->dim);
    for(int i = 0; i < this->dim; ++i) {
        out.data[i] = this->data[i] + that.data[i];
    }
    return out;
}

vec_t vec_t::operator-(const vec_t& that) const
{
    return (*this) + (that * -1.f);
}

std::ostream& operator<<(std::ostream& os, const vec_t& v)
{
    for(int i = 0; i < v.dim; ++i) {
        os << v[i] << " ";
    }
    return os;
}

std::istream& operator>>(std::istream& is, vec_t& v)
{
    int dim = 0;
    is >> dim;
    v.setdim(dim);

    for(int i = 0; i < dim; ++i) {
        is >> v.data[i];
    }
    return is;
}

double norm(const vec_t& v, const norm_t norm)
{
    auto euclid_norm = [](const vec_t& v) {
        double sum = 0;
        for(int i = 0; i < v.dim; ++i) {
            sum += v[i] * v[i];
        }
        return std::sqrt(sum);
    };

    switch(norm) {
        case EUCLID:
            return euclid_norm(v);
        default:
            return -1;
    }
}

struct sqmatrix_t
{
    int     dim;
    double  *data;

    sqmatrix_t();
    sqmatrix_t(const int dim);
    
    sqmatrix_t(const sqmatrix_t& that);
    sqmatrix_t(sqmatrix_t&& that);
    
    ~sqmatrix_t();

    sqmatrix_t& operator=(const sqmatrix_t& that);
    sqmatrix_t& operator=(sqmatrix_t&& that);

    void setdim(const int dim);

    double*         operator[](const int idx);
    const double*   operator[](const int idx) const;

    sqmatrix_t operator*(const float b) const;
    sqmatrix_t operator/(const float b) const;
    sqmatrix_t operator+(const sqmatrix_t& that) const;
    sqmatrix_t operator-(const sqmatrix_t& that) const;

    vec_t operator*(const vec_t& that) const;
};

std::ostream& operator<<(std::ostream& os, const sqmatrix_t& m);
std::istream& operator>>(std::istream& is,       sqmatrix_t& m);

sqmatrix_t::sqmatrix_t()
{
    this->dim = 0;
    this->data = nullptr;
}

sqmatrix_t::sqmatrix_t(const int dim)
{
    this->dim = dim;
    this->data = new double[this->dim * this->dim]{};
}

sqmatrix_t::sqmatrix_t(const sqmatrix_t& that)
{
    new(this) sqmatrix_t(that.dim);
    for(int i = 0; i < this->dim * this->dim; ++i) {
        this->data[i] = that.data[i];
    }
}

sqmatrix_t::sqmatrix_t(sqmatrix_t&& that)
{
    new(this) sqmatrix_t;
    std::swap(this->data, that.data);
    std::swap(this->dim,  that.dim);
}

sqmatrix_t::~sqmatrix_t()
{
    delete[] this->data;
}

sqmatrix_t& sqmatrix_t::operator=(const sqmatrix_t& that)
{
    if(this->dim != that.dim) {
        throw std::runtime_error("Non-compatible assignment");
    }

    for(int i = 0; i < this->dim * this->dim; ++i) {
        this->data[i] = that.data[i];
    }

    return *this;
}

sqmatrix_t& sqmatrix_t::operator=(sqmatrix_t&& that)
{
    if(this->dim != that.dim) {
        throw std::runtime_error("Non-compatible assignment");
    }

    std::swap(this->data, that.data);
    std::swap(this->dim,  that.dim);
    return *this;
}

void sqmatrix_t::setdim(const int dim)
{
    if((this->dim != dim) && (dim > 0)) {
        this->~sqmatrix_t();
        new(this) sqmatrix_t(dim);
    }
}

double* sqmatrix_t::operator[](const int idx)
{
    return this->data + this->dim * idx;
}

const double* sqmatrix_t::operator[](const int idx) const
{
    return this->data + this->dim * idx;
}

sqmatrix_t sqmatrix_t::operator*(const float b) const
{
    sqmatrix_t out(this->dim);

    for(int i = 0; i < out.dim * out.dim; ++i) {
        out.data[i] = this->data[i] * b;
    }
    return out;
}

sqmatrix_t sqmatrix_t::operator/(const float b) const
{
    return (*this) * (1 / b);
}

sqmatrix_t sqmatrix_t::operator+(const sqmatrix_t& that) const
{
    if(this->dim != that.dim) {
        throw std::runtime_error("Non-compatible binary operator");
    }
    
    sqmatrix_t out(this->dim);

    for(int i = 0; i < out.dim * out.dim; ++i) {
        out.data[i] = this->data[i] + that.data[i];
    }
    return out; 
}

sqmatrix_t sqmatrix_t::operator-(const sqmatrix_t& that) const
{
    return (*this) + (that * -1.f);
}

vec_t sqmatrix_t::operator*(const vec_t& that) const
{
    if(this->dim != that.dim) {
        throw std::runtime_error("Non-compatible binary operator");
    }

    vec_t out(this->dim);
    for(int i = 0; i < out.dim; ++i) {
        for(int j = 0; j < this->dim; ++j) {
            out.data[i] += (*this)[i][j] * that[j];
        }
    }
    return out;
}

std::ostream& operator<<(std::ostream& os, const sqmatrix_t& m)
{
    for(int i = 0; i < m.dim; ++i) {
        for(int j = 0; j < m.dim; ++j) {
            os << m[i][j] << " ";
        }
        os << std::endl;
    }
    return os;
}

std::istream& operator>>(std::istream& is, sqmatrix_t& m)
{
    int dim = 0;
    is >> dim;
    m.setdim(dim);

    for(int i = 0; i < m.dim; ++i) {
        for(int j = 0; j < m.dim; ++j) {
            is >> m[i][j];
        }
    }
    return is;
}

struct sole_t
{
    sqmatrix_t  A;
    vec_t       b;
    sole_t() = default;
    sole_t(std::string fname);
    ~sole_t() = default;
};

sole_t::sole_t(std::string fname)
{
    std::ifstream is(fname);
    if(!is.is_open()) {
        throw std::runtime_error("Can't open file [" + fname + "]");
    }
    is >> A;
    is.close();

    b.setdim(A.dim);

    std::random_device rd;
    std::mt19937 gen(rd());
    for(int i = 0; i < A.dim; ++i) {
        b[i] = std::generate_canonical<double, std::numeric_limits<double>::digits>(gen);
    }
    b = A * b;
}

vec_t solve_sor(const sole_t& sole,
                const vec_t& initial_guess = vec_t(),
                const double res_stop = 1e-6,
                const double w = 1.5,  
                std::ostream& os = std::cout,
                bool output = true)
{
    vec_t out = initial_guess;
    out.setdim(sole.A.dim);

    double res = norm(sole.A * out - sole.b);
    while(res > res_stop) {
        if(output) {
            os << "res = " << res << std::endl;
        }
        for(int i = 0; i < sole.A.dim; ++i) {
            double sigma = 0;
            for(int j = 0; j < sole.A.dim; ++j) {
                if(j == i) continue;
                sigma += sole.A[i][j] * out[j];
            }
            out[i] = (1 - w) * out[i] + (w / sole.A[i][i]) * (sole.b[i] - sigma);
        }
        res = norm(sole.A * out - sole.b);
    }
    return out;
}

vec_t solve_seidel( const sole_t& sole,
                    const vec_t& initial_guess = vec_t(),
                    const double res_stop = 1e-6,  
                    std::ostream& os = std::cout,
                    bool output = true)
{
    return solve_sor(sole, initial_guess, res_stop, 1.0, os, output);
}

int main(int argc, char **argv)
{
    if(argc != 2) {
        throw std::runtime_error("Wrong amount of arguments");
    }
    
    sole_t sole(argv[1]);

    std::cout << "SOR:" << std::endl;
    auto start = std::chrono::system_clock::now();
    vec_t out_sor = solve_sor(sole);
    auto stop = std::chrono::system_clock::now();
    double dur_sor = std::chrono::duration<double, std::milli>(stop - start).count();

    std::cout << "Seidel:" << std::endl;
    start = std::chrono::system_clock::now();
    vec_t out_seidel = solve_seidel(sole);
    stop = std::chrono::system_clock::now();
    double dur_seidel = std::chrono::duration<double, std::milli>(stop - start).count();

    std::cout << "Seidel: " << dur_seidel << "ms: " << out_seidel << std::endl
              << "SOR:    " << dur_sor    << "ms: " << out_sor    << std::endl;

    return 0;
}
